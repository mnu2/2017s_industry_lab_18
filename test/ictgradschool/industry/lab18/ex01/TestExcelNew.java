package ictgradschool.industry.lab18.ex01;

import org.junit.Before;
import org.junit.Test;

import java.io.FileNotFoundException;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * TODO Write tests
 */
public class TestExcelNew {

    private ExcelNew sheet;

    @Before public void setUP(){this.sheet = new ExcelNew();}

    @Test
    public void testGoodCode() {
        assertTrue(false);
    }


    @Test
    public void testsClassSize(){
        sheet.generateStudents();
        assertEquals(550, sheet.getOutput().length);
    }


    @Test
    public void testFirstNameReader(){
        assertEquals(19948, sheet.getFirstNameList().size());
    }
    @Test
    public void testSurnameReader(){
        assertEquals(87968, sheet.getSurnameList().size());
    }
}
