package ictgradschool.industry.lab18.ex01;

import java.io.*;
import java.util.*;

/**
 * TODO Please test & refactor this - my eyes are watering just looking at it :'(
 */
public class ExcelNew {

	private List<String> firstNameList = new ArrayList<String>();
	private List<String> surnameList = new ArrayList<String>();
	private int classSize = 550;
	private String output = "";
	private String student;

	public ExcelNew() {
		readFiles(firstNameList, "FirstNames.txt");
		readFiles(surnameList, "Surnames.txt");
	}

	public void readFiles(List<String> stuff, String fileName){
		String line;
		try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
			while ((line = br.readLine()) != null) {
				stuff.add(line);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	public void generateStudents() {
		for(int i = 1; i <= classSize; i++){
			student = "";
			setStudentId(i);
			setStudentName();
			//Student Skill
			int randStudentSkill = setRandNum(101, 0);
			//Labs//////////////////////////
			setExamMark(randStudentSkill, 5, 15, 25, 65, false);
			//Test/////////////////////////
			setExamMark(randStudentSkill, 5, 25, 65, 90, false);
			///////////////Exam////////////
			setExamMark(randStudentSkill, 7, 20, 60, 90, true);
			//////////////////////////////////
			student += "\n";
			output += student;
		}
	}

	private void writeToFile(String outputFile) {
		try(BufferedWriter bw = new BufferedWriter(new FileWriter(outputFile))){
			bw.write(output);
		}
		catch(IOException e){
			System.out.println(e);
		}
	}

	private  void setExamMark(int randStudentSkill, int lower, int lower_mid, int uppper_mid, int upper, boolean s) {
		if(randStudentSkill <= lower){
			int randDNSProb = setRandNum(101, 0);
			if(s && randDNSProb <= 5){
				student += ""; //DNS
			}else{
				student += setRandNum(40, 0); //[0,39]
			}
		} else if(randStudentSkill <= lower_mid){
			student +=setRandNum(10, 40); //[40,49]
		} else if(randStudentSkill <= uppper_mid){
			student += setRandNum(20, 50);//[50,69]
		} else if(randStudentSkill <= upper){
			student += setRandNum(20, 70); //[70,89]
		} else{
			student += setRandNum(11, 90); //[90,100]

		}
	}


	private static int setRandNum(int a, int b) {
		return (int)(Math.random()*a + b);
	}

	private void setStudentName() {
		int randFNIndex = (int)(Math.random()*firstNameList.size());
		int randSNIndex = (int)(Math.random()*surnameList.size());
		student += "\t" + surnameList.get(randSNIndex) + "\t" + firstNameList.get(randFNIndex) + "\t";

	}

	private void setStudentId(int i) {
		if(i/10 < 1){
			student += "000" + i;
		}else if (i/100 < 1){
			student += "00" + i;
		}else if (i/1000 < 1){
			student += "0"+i;
		}else{
			student += i;
		}
	}

	public void start(){
		generateStudents();
		writeToFile("Data_Out1.txt");
	}

	public String[] getOutput() {
		return output.split("\n");
	}

	public List<String> getFirstNameList() {
		return firstNameList;
	}

	public List<String> getSurnameList() {
		return surnameList;
	}

	public static void main(String [] args){

		ExcelNew excelNew = new ExcelNew();
		excelNew.start();

	}



}
