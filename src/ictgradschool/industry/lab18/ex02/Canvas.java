package ictgradschool.industry.lab18.ex02;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class Canvas extends JPanel{

    private List<Integer> xs2 = new ArrayList<>();
    private List<Integer> ys2 = new ArrayList<>();
    private List<Integer> xs1 = new ArrayList<>();
    private List<Integer> ys1 = new ArrayList<>();
    private int fx;
    private int fy;
    private boolean done;

    public Canvas(List<Integer> xs2, List<Integer> ys2, List<Integer> xs1, List<Integer> ys1, int fx, int fy, boolean done){
        this.fy = fy;
        this.fx = fx;
        this.ys1=  ys1;
        this.xs1 = xs1;
        this.done = done;
        this.xs2 = xs2;
        this.ys2 = ys2;
    }

    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        for (int i = 0; i < xs2.size(); i++) {
            g.setColor(Color.gray);
            g.fill3DRect(xs2.get(i) * 25 + 1, ys2.get(i) * 25 + 1, 25 - 2, 25 - 2, true);
        }
        createGreenSquare(g);
        createRedSquare(g);
        checkDoneStatus(g);
    }

    private void checkDoneStatus(Graphics g) {
        if (done) {
            g.setColor(Color.red);
            g.setFont(new Font("Arial", Font.BOLD, 60));
            FontMetrics fm = g.getFontMetrics();
            g.drawString("Over", (30 * 25 + 6 - fm.stringWidth("Over")) / 2, (20 * 25 + 28) / 2);
        }
    }

    private void createGreenSquare(Graphics g) {
        g.setColor(Color.green);
        g.fill3DRect(fx * 25 + 1, fy * 25 + 1, 25 - 2, 25 - 2, true);
    }

    private void createRedSquare(Graphics g) {
        for (int i = 0; i < xs1.size(); i++) {
            g.setColor(Color.red);
            g.fill3DRect(xs1.get(i) * 25 + 1, ys1.get(i) * 25 + 1, 25 - 2, 25 - 2, true);
        }
    }

    public void setProperties(List<Integer> xs2, List<Integer> ys2, List<Integer> xs1, List<Integer> ys1, int fx, int fy, boolean done){
        this.fy = fy;
        this.fx = fx;
        this.ys1=  ys1;
        this.xs1 = xs1;
        this.done = done;
        this.xs2 = xs2;
        this.ys2 = ys2;
    }
}

